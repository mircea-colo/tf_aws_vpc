resource "aws_vpc" "dev_vpc" {
  cidr_block           = "${var.cidr_block}"
  enable_dns_hostnames = "${var.dns_hostnames}"
  enable_dns_support   = "${var.dns_support}"

  tags {
    Name = "${var.name}"
  }
}

resource "aws_internet_gateway" "dev_igw" {
  vpc_id = "${aws_vpc.dev_vpc.id}"

  tags {
    Name = "${var.name}-igw"
  }
}

resource "aws_route" "dev_rt" {
  route_table_id         = "${aws_vpc.dev_vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.dev_igw.id}"
}

resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.dev_vpc.id}"
  cidr_block = "${var.subnet_public}"

  tags {
    Name = "${var.name}-public"
  }
}
