variable "name" {
  description = "VPC name"
}

variable "cidr_block" {
  description = "CIDR of the VPC"
}

variable "subnet_public" {
  description = "A public subnet"
}

variable "dns_hostnames" {
  description = "Toggle true to enable a private DNS within the VPC"
  default     = true
}

variable "dns_support" {
  description = "Toggle true to enable a private DNS within the VPC"
  default     = true
}

output "public_subnet_id" {
  value = "${aws_subnet.public.id}"
}

output "vpc_id" {
  value = "${aws_vpc.dev_vpc.id}"
}

output "cidr" {
  value = "${aws_vpc.dev_vpc.cidr_block}"
}
